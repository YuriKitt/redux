# Redux Counter Example

This README describes the logical steps to consider before creating a simple Redux store
for managing the state of a counter in a React application, as well as the steps to create the Redux store itself.

## Logical steps to consider before creating the Redux store

1. Identify the state shape:
   Analyze your application's requirements and decide on the shape of the state.
   For this example, we have a simple counter state with a single `counter` property.

2. Determine the actions:
   Identify the actions that can be performed on the state.
   In this example, we have two actions: increment and decrement the counter.

3. Plan the application structure:
   Organize your application into a logical folder structure.
   Typically, you will have separate folders for components, Redux store, action types, action creators, and reducers.

## Steps to create the Redux store

1. **Create action types**

   Define constants for your action types in a separate file, typically named `actionTypes.js`.

```javascript
export const SET_INCREMENT = 'SET_INCREMENT';
export const ADD_INCREMENT = 'ADD_INCREMENT';
export const ADD_DECREMENT = 'ADD_DECREMENT';
```

2. **Create action creators**

Define action creators in a separate file, usually named actionCreators.js.
Action creators are functions that return an action object with a type and an optional payload.

```javascript
import * as actions from './actionTypes';

export const ADD_INCREMENT = () => ({
  type: actions.ADD_INCREMENT,
});

export const ADD_DECREMENT = () => ({
  type: actions.ADD_DECREMENT,
});
```

3. **Create a reducer**

Define a reducer function that takes the current state and an action, 
and returns a new state based on the action's type and payload. 
The reducer is responsible for updating the state of your application in response to dispatched actions.

```javascript
import * as actions from './actionTypes';

const initialState = { counter: 0 };

const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADD_INCREMENT:
      return { counter: state.counter + 1 };

    case actions.ADD_DECREMENT:
      return { counter: state.counter - 1 };

    default:
      return state;
  }
};

export default Reducer;
```

4. Create the Redux store

Import the createStore function from the redux library and create a store by passing the reducer as its argument.

```javascript
import { createStore } from 'redux';

import counterReducer from './Counter';

const store = createStore(counterReducer);

export default store;
```

5. Connect the Redux store to your React application

Use the Provider component from the react-redux library to make the Redux store available to all components in your application. 
Access the state and dispatch actions using the useSelector and useDispatch hooks from react-redux.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './path/to/your/store';
import App from './App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  document.getElementById('root')
);
```
