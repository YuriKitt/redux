import { useSelector, useDispatch } from 'react-redux';

import classes from './Counter.module.css'; //css module React

// import store from '../../store'; //only for log store in conslole, not needed in a real project
import * as actions from '../../store/Counter/actionCreator';

export const Counter = () => {
  const dispatch = useDispatch();

  const counter = useSelector((state) => state.counter.counter); // useSelector - automatically creates subscription and manages unsubcription
  const step = useSelector((state) => state.counter.step); // useSelector - automatically creates subscription and manages unsubcription
  const show = useSelector((state) => state.counter.show); // useSelector - automatically creates subscription and manages unsubcription

  const setIncrementHandler = () => dispatch(actions.setIncrement());
  const incrementHandler = () => dispatch(actions.addIncrement()); //This function dispatches the ADD_INCREMENT action creator when called, increasing the counter by step value.
  const decrementHandler = () => dispatch(actions.addDecrement()); //This function dispatches the ADD_DECREMENT action creator when called, decreasing the counter by step value.

  const resetHandler = () => dispatch(actions.reset());

  const toggleHandler = () => dispatch(actions.toggleStep());

  // console.log(store.getState()); //for research only

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>
        <p>counter: {counter}</p>
        {show && <p>step:{step}</p>}
      </div>
      <div>
        <button onClick={incrementHandler}>Increse</button>
        <button onClick={decrementHandler}>Decrease</button>
      </div>
      <button onClick={setIncrementHandler}>Toggle step (1 or 5)</button>
      <button onClick={resetHandler}>Reset</button>
      <button onClick={toggleHandler}>Show/hide step</button>
    </main>
  );
};
