import { useSelector, useDispatch } from 'react-redux';

import classes from './Header.module.css';

import store from '../../store'; //only for log store in conslole, not needed in a real project
import * as actions from '../../store/Auth/actionCreator';

export const Header = () => {
  const dispatch = useDispatch();

  const auth = useSelector((store) => store.auth.auth);

  const loginHandler = () => dispatch(actions.toggleAuth());

  console.log(store.getState()); //for research only

  return (
    <header className={classes.header}>
      <h1>{auth ? 'Welcome!' : 'Redux'}</h1>
      <nav>
        <ul>
          <li>{auth && <a href="#">One</a>}</li>
          <li>{auth && <a href="#">Two</a>}</li>
          <li>
            <button onClick={loginHandler}>{auth ? 'Logout' : 'Login'}</button>
          </li>
        </ul>
      </nav>
    </header>
  );
};
