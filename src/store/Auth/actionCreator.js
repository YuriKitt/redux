// Action defining for dispatch

// Action creators are functions that return an action object containing a type and an optional payload.
// They are used to dispatch actions to the Redux store, which then updates the state based on the given action type and payload.

// First, you import the action types defined in the ./actionTypes module, and then you create the action creators.

import * as actionTypes from './actionTypes';

export const toggleAuth = (value) => ({
  type: actionTypes.TOGGLE_AUTH,
  payload: { value },
});
