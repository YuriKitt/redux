// Store creating
// The Redux store is responsible for holding the application's state and allows you to dispatch actions and subscribe to state changes.

// First, you import the createStore function from the redux library.
import { createStore, combineReducers } from 'redux';

// Then, you import the Reducer from the module, which is the reducer you defined in a previous steps.
import counterReducer from './Counter';
import authReducer from './Auth';

// Next, you call the createStore function and pass counterReducer as its argument.
// This creates a new Redux store with the given reducer responsible for handling state updates.
// const store = createStore(counterReducer);

const rootReducer = combineReducers({
    counter: counterReducer,
	auth: authReducer,
    
});

const store = createStore(rootReducer);

export default store;
