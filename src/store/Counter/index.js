// defining a Redux reducer

// First, you import the action types defined in the ./actionTypes module.
// Then, you define the initialState object

import * as actionTypes from './actionTypes';

const initialState = {
  counter: 0,
  step: 1,
  show: true,
};

// The Reducer function takes two arguments: state and action.
// The state argument has a default value of initialState so that when the reducer is called for the first time,
// it initializes the state with the given initialState object.

// A reducer is a pure function that takes the current state and an action, and returns a new state based on the action's type and payload.
// The reducer is responsible for updating the state of your application in response to dispatched actions.

const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_INCREMENT:
      return {
        ...state,
        counter: state.counter + state.step,
      };

    case actionTypes.ADD_DECREMENT:
      return {
        ...state,
        counter: state.counter - state.step,
      };

    case actionTypes.SET_INCREMENT:
      return {
        ...state,
        step: state.step === 1 ? 5 : 1,
      };

    case actionTypes.TOGGLE_STEP:
      return {
        ...state,
        show: !state.show,
      };

    case actionTypes.RESET:
      return initialState;

    default:
      return state;
  }
};

export default Reducer;

// This reducer can then be combined with other reducers (if any) using the combineReducers function provided by Redux
// and passed to the createStore function to create the Redux store for your application.
