// Action types (define constants for naming)

// In general, the strings inside the single quotes (' ') are just used as unique identifiers for the action types. 
// They can be any string, as long as they are unique within your application. 
// The main purpose of these constants is to help you avoid errors due to typos and to make your code more readable and maintainable.

//It is a common practice to use uppercase strings with underscores to separate words for action types, 
// as it makes them visually distinct and easy to read. 
// However, this is just a convention, and you can use any string format that works for you, 
// as long as it is unique and consistent within your application.

export const SET_INCREMENT = 'SET_INCREMENT';
export const ADD_INCREMENT = 'ADD_INCREMENT';
export const ADD_DECREMENT = 'ADD_DECREMENT';

export const RESET = 'RESET';
export const TOGGLE_STEP = 'TOGGLE_STEP';