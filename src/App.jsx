import { Counter, Header } from './components';
import { useSelector } from 'react-redux';

import './App.css';

function App() {
  const auth = useSelector((store) => store.auth.auth);
  return (
    <div className="App">
      <Header />
      {auth && <Counter />}
    </div>
  );
}

export default App;
